import 'package:flutter/material.dart';
import 'package:fsel_quick_note_ver_2/widgets/reusable_widgets.dart';

class ListViewContainer extends StatelessWidget {
  const ListViewContainer({
    Key? key,
    required this.title,
    required this.content,
  }) : super(key: key);

  final String title;
  final String content;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 80,
      margin: EdgeInsets.only(bottom: 20),
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.yellow[200],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            reusableTextWidget(text: title, fontWeight: FontWeight.bold),
            reusableTextWidget(text: content),
          ],
      ),
    );
  }
}
