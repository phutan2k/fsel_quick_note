import 'package:flutter/material.dart';

Widget reusableTextWidget({
  required String text,
  Color color = const Color(0xFF000000),
  double size = 14,
  FontWeight fontWeight = FontWeight.w400,
  TextOverflow overflow = TextOverflow.ellipsis,
}) {
  return Text(
    text,
    overflow: overflow,
    style: TextStyle(
      color: color,
      fontSize: size,
      fontWeight: fontWeight,
    ),
  );
}

Widget reusableIconWidget({
  required IconData icon,
  Color backgroundColor = const Color(0x00000000),
  Color iconColor = const Color(0xFF000000),
  double size = 40,
  double iconSize = 16,
}) {
  return Container(
    width: size,
    height: size,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(size / 2),
      color: backgroundColor,
    ),
    child: Icon(
      icon,
      color: iconColor,
      size: iconSize,
    ),
  );
}
