import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fsel_quick_note_ver_2/blocs/quick_note_blocs.dart';
import 'package:fsel_quick_note_ver_2/blocs/quick_note_states.dart';
import 'package:fsel_quick_note_ver_2/screens/detail_home_page/detail_home_page.dart';
import 'package:fsel_quick_note_ver_2/widgets/home_page_widgets.dart';
import 'package:fsel_quick_note_ver_2/widgets/reusable_widgets.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: SafeArea(
        child: Scaffold(
            appBar: AppBar(
              centerTitle: true,
              title: Text('Quick Note'),
            ),
            body: Stack(
              children: [
                Positioned(
                  left: 0,
                  right: 0,
                  top: 0,
                  bottom: 0,
                  child: Column(
                    children: [
                      BlocBuilder<QuickNoteBloc, QuickNoteStates>(
                        builder: (context, state) {
                          return Expanded(
                            child: Container(
                              margin: EdgeInsets.symmetric(horizontal: 10),
                              child: ListView.builder(
                                  itemCount: state.notes.length,
                                  itemBuilder: (_, index) {
                                    return GestureDetector(
                                      onTap: () => Navigator.of(context).push(
                                        MaterialPageRoute(
                                          builder: (context) => DetailHomePage(
                                            quickNoteModel: state.notes[index],
                                          ),
                                        ),
                                      ),
                                      child: ListViewContainer(
                                        title: state.notes[index].title,
                                        content: state.notes[index].content,
                                      ),
                                    );
                                  }),
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ),
                Positioned(
                  bottom: 50,
                  right: 20,
                  child: GestureDetector(
                    onTap: () => Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => DetailHomePage())),
                    child: Container(
                      width: 50,
                      height: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        color: Colors.yellow[600],
                      ),
                      child: Center(
                        child: reusableTextWidget(
                          text: '+',
                          color: Colors.white,
                          size: 30,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )),
      ),
    );
  }
}
