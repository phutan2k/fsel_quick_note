import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fsel_quick_note_ver_2/blocs/quick_note_blocs.dart';
import 'package:fsel_quick_note_ver_2/blocs/quick_note_events.dart';
import 'package:fsel_quick_note_ver_2/models/quick_note.dart';
import 'package:fsel_quick_note_ver_2/widgets/detail_home_page_widgets.dart';
import 'package:fsel_quick_note_ver_2/widgets/reusable_widgets.dart';
import 'package:image_picker/image_picker.dart';

class DetailHomePage extends StatefulWidget {
  const DetailHomePage({Key? key, this.quickNoteModel}) : super(key: key);
  final QuickNoteModel? quickNoteModel;

  @override
  State<DetailHomePage> createState() => _DetailHomePageState();
}

class _DetailHomePageState extends State<DetailHomePage> {
  late String _pickedImage;
  late TextEditingController titleController;
  late TextEditingController contentController;

  @override
  void initState() {
    super.initState();

    // Xét giá trị khi vào detail
    _pickedImage = widget.quickNoteModel?.filePath ?? '';
    titleController =
        TextEditingController(text: widget.quickNoteModel?.title ?? '');
    contentController =
        TextEditingController(text: widget.quickNoteModel?.content ?? '');
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.yellow[100],
      child: SafeArea(
        child: Scaffold(
          body: Container(
            color: Colors.yellow[100],
            child: Column(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 65,
                  padding: EdgeInsets.only(right: 15),
                  child: Row(
                    children: [
                      GestureDetector(
                        onTap: () {
                          if (titleController.value.text.isEmpty) {
                            Navigator.of(context).pop();
                          } else {
                            if (widget.quickNoteModel == null) {
                              BlocProvider.of<QuickNoteBloc>(context)
                                  .add(CreateNoteEvent(QuickNoteModel(
                                id: 0,
                                title: titleController.value.text,
                                content: contentController.value.text,
                                filePath: _pickedImage,
                              )));
                            } else {
                              BlocProvider.of<QuickNoteBloc>(context)
                                  .add(EditNoteEvent(QuickNoteModel(
                                id: widget.quickNoteModel!.id,
                                title: titleController.value.text,
                                content: contentController.value.text,
                                filePath: _pickedImage,
                              )));
                            }
                            Navigator.of(context).pop();
                          }
                        },
                        child: reusableIconWidget(
                            icon: Icons.arrow_back_ios_new_outlined),
                      ),
                      reusableTextWidget(
                          text: 'Quick Note details',
                          size: 20,
                          fontWeight: FontWeight.bold),
                      Expanded(child: SizedBox()),
                      GestureDetector(
                        onTap: () {
                          BlocProvider.of<QuickNoteBloc>(context)
                              .add(DeleteNoteEvent(widget.quickNoteModel!.id));
                          Navigator.of(context).pop();
                        },
                        child: reusableTextWidget(
                          text: 'Delete',
                          color: Colors.red,
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Stack(
                    children: [
                      Positioned(
                        left: 0,
                        right: 0,
                        bottom: 0,
                        top: 0,
                        child: SingleChildScrollView(
                          child: Container(
                            height: MediaQuery.of(context).size.height,
                            color: Colors.yellow[50],
                            padding: EdgeInsets.symmetric(
                                vertical: 10, horizontal: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                BuildTextFieldWidget(
                                  controller: titleController,
                                  hintText: 'Title',
                                  isTitle: true,
                                ),
                                _pickedImage.isNotEmpty
                                    ? Container(
                                        child: Image.file(
                                          File(_pickedImage),
                                          fit: BoxFit.cover,
                                          width: double.maxFinite,
                                          height: 200,
                                        ),
                                      )
                                    : Container(),
                                Expanded(
                                  child: BuildTextFieldWidget(
                                    controller: contentController,
                                    hintText: 'Content',
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                        bottom: 0,
                        left: 0,
                        right: 0,
                        child: Container(
                          height: 65,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                              topRight: Radius.circular(20),
                            ),
                            color: Colors.yellow[100],
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              GestureDetector(
                                onTap: () {},
                                child: reusableIconWidget(
                                    icon: Icons.record_voice_over_outlined,
                                    iconSize: 35),
                              ),
                              GestureDetector(
                                onTap: () {
                                  _takePhoto();
                                },
                                child: reusableIconWidget(
                                  icon: Icons.camera_alt_outlined,
                                  iconSize: 35,
                                ),
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _takePhoto() async {
    await _pickImage(ImageSource.camera);
  }

  Future<void> _pickImage(ImageSource source) async {
    final picker = ImagePicker();
    final pickedImage = await picker.pickImage(source: source);

    if (pickedImage != null) {
      _pickedImage = pickedImage.path;
    }
  }
}
