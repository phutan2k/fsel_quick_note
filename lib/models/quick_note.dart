import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:json_annotation/json_annotation.dart';

// Đặt tên cho file generated
part 'quick_note.g.dart';

@JsonSerializable()
@CopyWith()
class QuickNoteModel {
  final int id;
  final String title;
  final String content;
  final String filePath;

  QuickNoteModel({required this.id, required this.title, this.content = '', this.filePath = ''});

  factory QuickNoteModel.fromJson(Map<String, dynamic> json) =>
      _$QuickNoteModelFromJson(json);

  Map<String, dynamic> toJson() => _$QuickNoteModelToJson(this);
}