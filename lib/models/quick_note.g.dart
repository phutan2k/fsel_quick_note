// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'quick_note.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

abstract class _$QuickNoteModelCWProxy {
  QuickNoteModel content(String content);

  QuickNoteModel filePath(String filePath);

  QuickNoteModel id(int id);

  QuickNoteModel title(String title);

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `QuickNoteModel(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// QuickNoteModel(...).copyWith(id: 12, name: "My name")
  /// ````
  QuickNoteModel call({
    String? content,
    String? filePath,
    int? id,
    String? title,
  });
}

/// Proxy class for `copyWith` functionality. This is a callable class and can be used as follows: `instanceOfQuickNoteModel.copyWith(...)`. Additionally contains functions for specific fields e.g. `instanceOfQuickNoteModel.copyWith.fieldName(...)`
class _$QuickNoteModelCWProxyImpl implements _$QuickNoteModelCWProxy {
  final QuickNoteModel _value;

  const _$QuickNoteModelCWProxyImpl(this._value);

  @override
  QuickNoteModel content(String content) => this(content: content);

  @override
  QuickNoteModel filePath(String filePath) => this(filePath: filePath);

  @override
  QuickNoteModel id(int id) => this(id: id);

  @override
  QuickNoteModel title(String title) => this(title: title);

  @override

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `QuickNoteModel(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// QuickNoteModel(...).copyWith(id: 12, name: "My name")
  /// ````
  QuickNoteModel call({
    Object? content = const $CopyWithPlaceholder(),
    Object? filePath = const $CopyWithPlaceholder(),
    Object? id = const $CopyWithPlaceholder(),
    Object? title = const $CopyWithPlaceholder(),
  }) {
    return QuickNoteModel(
      content: content == const $CopyWithPlaceholder() || content == null
          ? _value.content
          // ignore: cast_nullable_to_non_nullable
          : content as String,
      filePath: filePath == const $CopyWithPlaceholder() || filePath == null
          ? _value.filePath
          // ignore: cast_nullable_to_non_nullable
          : filePath as String,
      id: id == const $CopyWithPlaceholder() || id == null
          ? _value.id
          // ignore: cast_nullable_to_non_nullable
          : id as int,
      title: title == const $CopyWithPlaceholder() || title == null
          ? _value.title
          // ignore: cast_nullable_to_non_nullable
          : title as String,
    );
  }
}

extension $QuickNoteModelCopyWith on QuickNoteModel {
  /// Returns a callable class that can be used as follows: `instanceOfQuickNoteModel.copyWith(...)` or like so:`instanceOfQuickNoteModel.copyWith.fieldName(...)`.
  // ignore: library_private_types_in_public_api
  _$QuickNoteModelCWProxy get copyWith => _$QuickNoteModelCWProxyImpl(this);
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

QuickNoteModel _$QuickNoteModelFromJson(Map<String, dynamic> json) =>
    QuickNoteModel(
      id: json['id'] as int,
      title: json['title'] as String,
      content: json['content'] as String? ?? '',
      filePath: json['filePath'] as String? ?? '',
    );

Map<String, dynamic> _$QuickNoteModelToJson(QuickNoteModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'content': instance.content,
      'filePath': instance.filePath,
    };
