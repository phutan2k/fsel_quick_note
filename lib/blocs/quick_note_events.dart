import 'package:equatable/equatable.dart';
import 'package:fsel_quick_note_ver_2/models/quick_note.dart';

sealed class QuickNoteEvents extends Equatable {
  QuickNoteEvents();

  @override
  List<Object?> get props => [];
}

class CreateNoteEvent extends QuickNoteEvents {
  final QuickNoteModel quickNoteModel;

  CreateNoteEvent(this.quickNoteModel);

  @override
  List<Object?> get props => [];
}

class EditNoteEvent extends QuickNoteEvents {
  final QuickNoteModel quickNoteModel;

  EditNoteEvent(this.quickNoteModel);

  @override
  List<Object?> get props => [];
}

class DeleteNoteEvent extends QuickNoteEvents {
  final int id;

  DeleteNoteEvent(this.id);

  @override
  List<Object?> get props => [];
}

class TakePhotoEvent extends QuickNoteEvents {
  final String filePath;

  TakePhotoEvent(this.filePath);

  @override
  List<Object?> get props => [];
}