import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:fsel_quick_note_ver_2/models/quick_note.dart';
import 'package:json_annotation/json_annotation.dart';

part 'quick_note_states.g.dart';

enum QuickNoteStatus { init, loading, success, fail }

@CopyWith()
@JsonSerializable()
class QuickNoteStates extends Equatable {
  final List<QuickNoteModel> notes;
  final QuickNoteStatus createNoteStatus;
  final QuickNoteStatus editNoteStatus;
  final QuickNoteStatus deleteNoteStatus;
  final QuickNoteStatus takePhotoStatus;

  QuickNoteStates({
    this.notes = const [],
    this.createNoteStatus = QuickNoteStatus.init,
    this.editNoteStatus = QuickNoteStatus.init,
    this.deleteNoteStatus = QuickNoteStatus.init,
    this.takePhotoStatus = QuickNoteStatus.init,
  });

  @override
  List<Object?> get props => [
        notes,
        createNoteStatus,
        editNoteStatus,
        deleteNoteStatus,
        takePhotoStatus,
      ];

  factory QuickNoteStates.fromJson(Map<String, dynamic> json) =>
      _$QuickNoteStatesFromJson(json);

  Map<String, dynamic> toJson() => _$QuickNoteStatesToJson(this);
}
