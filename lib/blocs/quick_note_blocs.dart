import 'dart:async';

import 'package:fsel_quick_note_ver_2/blocs/quick_note_events.dart';
import 'package:fsel_quick_note_ver_2/blocs/quick_note_states.dart';
import 'package:fsel_quick_note_ver_2/models/quick_note.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';

class QuickNoteBloc extends HydratedBloc<QuickNoteEvents, QuickNoteStates> {
  QuickNoteBloc() : super(QuickNoteStates()) {
    on<CreateNoteEvent>(_createNoteEvent);
    on<EditNoteEvent>(_editNoteEvent);
    on<DeleteNoteEvent>(_deleteNoteEvent);
    on<TakePhotoEvent>(_takePhotoEvent);
  }

  @override
  QuickNoteStates? fromJson(Map<String, dynamic> json) {
    return QuickNoteStates.fromJson(json);
  }

  @override
  Map<String, dynamic>? toJson(QuickNoteStates state) {
    return state.toJson();
  }

  FutureOr<void> _createNoteEvent(
    CreateNoteEvent event,
    Emitter<QuickNoteStates> emit,
  ) {
    emit(state.copyWith(createNoteStatus: QuickNoteStatus.loading));

    final notes = [...state.notes];

    notes.add(QuickNoteModel(
      id: notes.length + 1,
      title: event.quickNoteModel.title,
      content: event.quickNoteModel.content,
      filePath: event.quickNoteModel.filePath,
    ));

    emit(state.copyWith(
        notes: notes, createNoteStatus: QuickNoteStatus.success));
  }

  FutureOr<void> _editNoteEvent(
    EditNoteEvent event,
    Emitter<QuickNoteStates> emit,
  ) {
    emit(state.copyWith(editNoteStatus: QuickNoteStatus.loading));

    final notes = [...state.notes];
    final index =
        notes.indexWhere((element) => element.id == event.quickNoteModel.id);

    if (index != -1) {
      notes[index] = QuickNoteModel(
        id: event.quickNoteModel.id,
        title: event.quickNoteModel.title,
        content: event.quickNoteModel.content,
        filePath: event.quickNoteModel.filePath,
      );
    }

    emit(state.copyWith(notes: notes, editNoteStatus: QuickNoteStatus.success));
  }

  FutureOr<void> _deleteNoteEvent(
    DeleteNoteEvent event,
    Emitter<QuickNoteStates> emit,
  ) {
    emit(state.copyWith(deleteNoteStatus: QuickNoteStatus.loading));

    final notes = [...state.notes];
    final index = notes.indexWhere((element) => element.id == event.id);

    if(index != -1) {
      notes.remove(notes[index]);
    }

    emit(state.copyWith(notes: notes, deleteNoteStatus: QuickNoteStatus.success));
  }

  FutureOr<void> _takePhotoEvent(
    TakePhotoEvent event,
    Emitter<QuickNoteStates> emit,
  ) {}
}
