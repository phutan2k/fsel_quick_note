// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'quick_note_states.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

abstract class _$QuickNoteStatesCWProxy {
  QuickNoteStates createNoteStatus(QuickNoteStatus createNoteStatus);

  QuickNoteStates deleteNoteStatus(QuickNoteStatus deleteNoteStatus);

  QuickNoteStates editNoteStatus(QuickNoteStatus editNoteStatus);

  QuickNoteStates notes(List<QuickNoteModel> notes);

  QuickNoteStates takePhotoStatus(QuickNoteStatus takePhotoStatus);

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `QuickNoteStates(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// QuickNoteStates(...).copyWith(id: 12, name: "My name")
  /// ````
  QuickNoteStates call({
    QuickNoteStatus? createNoteStatus,
    QuickNoteStatus? deleteNoteStatus,
    QuickNoteStatus? editNoteStatus,
    List<QuickNoteModel>? notes,
    QuickNoteStatus? takePhotoStatus,
  });
}

/// Proxy class for `copyWith` functionality. This is a callable class and can be used as follows: `instanceOfQuickNoteStates.copyWith(...)`. Additionally contains functions for specific fields e.g. `instanceOfQuickNoteStates.copyWith.fieldName(...)`
class _$QuickNoteStatesCWProxyImpl implements _$QuickNoteStatesCWProxy {
  final QuickNoteStates _value;

  const _$QuickNoteStatesCWProxyImpl(this._value);

  @override
  QuickNoteStates createNoteStatus(QuickNoteStatus createNoteStatus) =>
      this(createNoteStatus: createNoteStatus);

  @override
  QuickNoteStates deleteNoteStatus(QuickNoteStatus deleteNoteStatus) =>
      this(deleteNoteStatus: deleteNoteStatus);

  @override
  QuickNoteStates editNoteStatus(QuickNoteStatus editNoteStatus) =>
      this(editNoteStatus: editNoteStatus);

  @override
  QuickNoteStates notes(List<QuickNoteModel> notes) => this(notes: notes);

  @override
  QuickNoteStates takePhotoStatus(QuickNoteStatus takePhotoStatus) =>
      this(takePhotoStatus: takePhotoStatus);

  @override

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `QuickNoteStates(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// QuickNoteStates(...).copyWith(id: 12, name: "My name")
  /// ````
  QuickNoteStates call({
    Object? createNoteStatus = const $CopyWithPlaceholder(),
    Object? deleteNoteStatus = const $CopyWithPlaceholder(),
    Object? editNoteStatus = const $CopyWithPlaceholder(),
    Object? notes = const $CopyWithPlaceholder(),
    Object? takePhotoStatus = const $CopyWithPlaceholder(),
  }) {
    return QuickNoteStates(
      createNoteStatus: createNoteStatus == const $CopyWithPlaceholder() ||
              createNoteStatus == null
          ? _value.createNoteStatus
          // ignore: cast_nullable_to_non_nullable
          : createNoteStatus as QuickNoteStatus,
      deleteNoteStatus: deleteNoteStatus == const $CopyWithPlaceholder() ||
              deleteNoteStatus == null
          ? _value.deleteNoteStatus
          // ignore: cast_nullable_to_non_nullable
          : deleteNoteStatus as QuickNoteStatus,
      editNoteStatus: editNoteStatus == const $CopyWithPlaceholder() ||
              editNoteStatus == null
          ? _value.editNoteStatus
          // ignore: cast_nullable_to_non_nullable
          : editNoteStatus as QuickNoteStatus,
      notes: notes == const $CopyWithPlaceholder() || notes == null
          ? _value.notes
          // ignore: cast_nullable_to_non_nullable
          : notes as List<QuickNoteModel>,
      takePhotoStatus: takePhotoStatus == const $CopyWithPlaceholder() ||
              takePhotoStatus == null
          ? _value.takePhotoStatus
          // ignore: cast_nullable_to_non_nullable
          : takePhotoStatus as QuickNoteStatus,
    );
  }
}

extension $QuickNoteStatesCopyWith on QuickNoteStates {
  /// Returns a callable class that can be used as follows: `instanceOfQuickNoteStates.copyWith(...)` or like so:`instanceOfQuickNoteStates.copyWith.fieldName(...)`.
  // ignore: library_private_types_in_public_api
  _$QuickNoteStatesCWProxy get copyWith => _$QuickNoteStatesCWProxyImpl(this);
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

QuickNoteStates _$QuickNoteStatesFromJson(Map<String, dynamic> json) =>
    QuickNoteStates(
      notes: (json['notes'] as List<dynamic>?)
              ?.map((e) => QuickNoteModel.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [],
      createNoteStatus: $enumDecodeNullable(
              _$QuickNoteStatusEnumMap, json['createNoteStatus']) ??
          QuickNoteStatus.init,
      editNoteStatus: $enumDecodeNullable(
              _$QuickNoteStatusEnumMap, json['editNoteStatus']) ??
          QuickNoteStatus.init,
      deleteNoteStatus: $enumDecodeNullable(
              _$QuickNoteStatusEnumMap, json['deleteNoteStatus']) ??
          QuickNoteStatus.init,
      takePhotoStatus: $enumDecodeNullable(
              _$QuickNoteStatusEnumMap, json['takePhotoStatus']) ??
          QuickNoteStatus.init,
    );

Map<String, dynamic> _$QuickNoteStatesToJson(QuickNoteStates instance) =>
    <String, dynamic>{
      'notes': instance.notes,
      'createNoteStatus': _$QuickNoteStatusEnumMap[instance.createNoteStatus]!,
      'editNoteStatus': _$QuickNoteStatusEnumMap[instance.editNoteStatus]!,
      'deleteNoteStatus': _$QuickNoteStatusEnumMap[instance.deleteNoteStatus]!,
      'takePhotoStatus': _$QuickNoteStatusEnumMap[instance.takePhotoStatus]!,
    };

const _$QuickNoteStatusEnumMap = {
  QuickNoteStatus.init: 'init',
  QuickNoteStatus.loading: 'loading',
  QuickNoteStatus.success: 'success',
  QuickNoteStatus.fail: 'fail',
};
